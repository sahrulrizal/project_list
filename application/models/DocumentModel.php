<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentModel extends CI_Model {

	private $t = 'document';
	private $t_list = 'document_list';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) {
            redirect('/');
        }
	}
	
	// ~Document

	public function getDocument()
	{
		$q = $this->db->get($this->t);
		return $q;
	}

	public function getDocumentID($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$q = $this->db->get_where($this->t,['id' => $id]);
		return $q;
	}

	public function getDocumentPC($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id_pc');
		}

		$this->db->join($this->t.' d', 'd.id = dl.id_document', 'inner');
		$this->db->where('dl.id_project_list', $id);
		$q = $this->db->get($this->t_list.' dl');
		return $q;
	}

	public function getDocumentAll($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$id = explode(',', $id);
		$this->db->where_in('id', $id);
		$q = $this->db->get($this->t);
		return $q;
	}

	public function inDocument($object='')
	{

		if ($object == '') {

			$object = [
				'activity' => $this->input->post('activity'),
				'id_mitra' => $this->input->post('mitra'),
				'start_target' => $this->input->post('start_target'),
				'end_target' => $this->input->post('end_target'),
				'created_date' => date('Y-m-d H:i:s')
			];

		}


		$q = $this->db->insert($this->t, $object);
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function upDocument($id='',$object='')
	{
		// Deifinis		
		if ($id == '') {
			$id = $this->input->post('id');
		}

		if ($object == '') {
			$object = array();
		}

		for ($i=0; $i < count($id) ; $i++) { 

			$activity 	= $this->input->post('activity')[$i];
			$mitra 	= $this->input->post('mitra')[$i];
			$start_target 	= $this->input->post('start_target')[$i];
			$end_target 	= $this->input->post('end_target')[$i];

			$arr = array(
				'id' => $id[$i],
				'activity' => $activity,
				'id_mitra' => $mitra,
				'start_target' => $start_target,
				'end_target' => $end_target,

			);

			array_push($object,$arr);

		}

		$q = $this->db->update_batch($this->t, $object,'id');
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function deDocument($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			$id = explode(',', $id);
			$this->db->where_in('id', $id);
			$q = $this->db->delete($this->t);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

	// ~Document LIST

	public function getDocumentList($idPc='')
	{
		if ($idPc == '') {
			$idPc = $this->input->get('idPc');
		}

		$q = $this->db->get_where($this->t_list,['id_project_list' => $idPc]);
		return $q;
	}

	public function getDocumentListID($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$q = $this->db->get_where($this->t_list,['id' => $id]);
		return $q;
	}

	public function inDocumentList($object='')
	{
		if ($object == '') {

			$object = [
				'id_document' => $this->input->post('id_document'),
				'id_project_list' => $this->input->post('id_project_list'),
				'created_date' => date('Y-m-d H:i:s')
			];

		}

		$q = $this->db->insert($this->t_list, $object);
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function upDocumentList($id='',$object='')
	{
		// Deifinis		
		if ($id == '') {
			$id = $this->input->get('id');
		}

		if ($obj == '') {
			$object = array();
		}

		for ($i=0; $i < count($id) ; $i++) { 

			$id_document = $this->input->post('id_document')[$i];
			$id_project_list = $this->input->post('id_project_list')[$i];

			$arr = array(
				'id' => $id[$i],
				'id_document' => $id_document,
				'id_project_list' => $id_project_list,
			);

			array_push($object,$arr);

		}

		$q = $this->db->update_batch($this->t_list, $object,'id');
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function deDocumentList($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			
			$id = explode(',', $id);
			$this->db->where_in('id', $id);
			$q = $this->db->delete($this->t_list);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

}

/* End of file M_pc.php */
/* Location: ./application/models/M_pc.php */

/* End of file pc.php */
/* Location: ./application/models/pc.php */