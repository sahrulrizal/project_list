<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MitraModel extends CI_Model {

	private $t = 'mitra';
	private $t_list = 'mitra_list';
	
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) {
            redirect('/');
        }
	}

	// ~MITRA

	public function getMitra()
	{
		$q = $this->db->get($this->t);
		return $q;
	}

	public function getMitraID($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$q = $this->db->get_where($this->t,['id' => $id]);
		return $q;
	}

	public function cekStatus($id='',$idpl='')
	{
		if ($id != '' && $idpl != '') {
			$get = $this->db->get_where('mitra_list', ['id_mitra' => $id, 'id_project_list' => $idpl]);
			if ($get->num_rows() > 0) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function getMitraAll($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$id = explode(',', $id);
		$this->db->where_in('id', $id);
		$q = $this->db->get($this->t);
		return $q;
	}

	public function inMitra($object='')
	{

		if ($object == '') {

			$object = [
				'mitra' => $this->input->post('mitra'),
				'pm' => $this->input->post('pm'),
				'phone' => $this->input->post('phone'),
				'created_date' => date('Y-m-d H:i:s')
			];

		}


		$q = $this->db->insert($this->t, $object);
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function upMitra($id='',$object='')
	{
		// Deifinis		
		if ($id == '') {
			$id = $this->input->post('id');
		}

		if ($object == '') {
			$object = array();
		}

		for ($i=0; $i < count($id) ; $i++) { 

			$mitra 	= $this->input->post('mitra')[$i];
			$pm 	= $this->input->post('pm')[$i];
			$phone 	= $this->input->post('phone')[$i];

			$arr = array(
				'id' => $id[$i],
				'mitra' => $mitra,
				'pm' => $pm,
				'phone' => $phone
			);

			array_push($object,$arr);

		}

		$q = $this->db->update_batch($this->t, $object,'id');
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function deMitra($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			$id = explode(',', $id);
			$this->db->where_in('id', $id);
			$q = $this->db->delete($this->t);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

	// ~MITRA LIST

	public function getMitraList($idPc='')
	{
		if ($idPc == '') {
			$idPc = $this->input->get('idPc');
		}

		$q = $this->db->get_where($this->t_list,['id_project_list' => $idPc]);
		return $q;
	}

	public function getMitraListID($id='',$arr='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		if ($arr == '') {
			$arr = ['id' => $id];
		}

		$q = $this->db->get_where($this->t_list,$arr);
		return $q;
	}

	public function inMitraList($object='')
	{
		if ($object == '') {

			$object = [
				'id_mitra' => $this->input->post('id_mitra'),
				'id_project_list' => $this->input->post('id_project_list'),
				'created_date' => date('Y-m-d H:i:s')
			];

		}

		$q = $this->db->insert($this->t_list, $object);
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function upMitraList($id='',$object='')
	{
		// Deifinis		
		if ($id == '') {
			$id = $this->input->get('id');
		}

		if ($obj == '') {
			$object = array();
		}

		for ($i=0; $i < count($id) ; $i++) { 

			$id_mitra = $this->input->post('id_mitra')[$i];
			$id_project_list = $this->input->post('id_project_list')[$i];

			$arr = array(
				'id' => $id[$i],
				'id_mitra' => $id_mitra,
				'id_project_list' => $id_project_list,
			);

			array_push($object,$arr);

		}

		$q = $this->db->update_batch($this->t_list, $object,'id');
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function deMitraList($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			
			$id = explode(',', $id);
			$this->db->where_in('id', $id);
			$q = $this->db->delete($this->t_list);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

	public function deMitraListNormal($obj='')
	{
		

		if ($obj == '') {
			$obj = ['id' => $id];
		}
		
		if ($obj != '') {
			
			$q = $this->db->delete($this->t_list,$obj);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

}

/* End of file M_pc.php */
/* Location: ./application/models/M_pc.php */

/* End of file pc.php */
/* Location: ./application/models/pc.php */