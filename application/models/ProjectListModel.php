<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ProjectListModel extends CI_Model
{

    public $tabel = 'pl';
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function se($status = '')
    {
        if ($status == '') {
            $status = $this->input->get('status');
        }

        if ($status != '') {
            $this->db->where('s_sts', $status);
        }

        $this->db->order_by('id', 'desc');
        $query = $this->db->get($this->tabel);
        return $query;
    }

    public function in()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tanggal = date("Y-m-d H-i-s");

        $task = $this->input->post('task');
        // $s_sts = $this->input->post('s_sts');
        // $s_tcel = $this->input->post('s_tcel');
        // $ket_s_sts = $this->input->post('ket_s_sts');
        // $ket_s_tcel = $this->input->post('ket_s_tcel');
        $status = $this->input->post('status');
        $pic_sts = $this->input->post('pic_sts');
        $pic_tcel = $this->input->post('pic_tcel');
        $note = $this->input->post('note');
        $review = $this->input->post('review');
        $confirm = $this->input->post('confirm');
        $via = $this->input->post('via');

        $data = array(
            'task' => $task,
            // 's_sts' => $s_sts,
            // 's_tcel' => $s_tcel,
            // 'ket_s_sts' => $ket_s_sts,
            // 'ket_s_tcel' => $ket_s_tcel,
            'status' => $status,
            'pic_sts' => $pic_sts,
            'pic_tcel' => $pic_tcel,
            'note' => $note,
            'review' => $review,
            'confirm' => $confirm,
            'via' => $via,
            'tanggal' => $tanggal,
        );

        $in = $this->db->insert($this->tabel, $data);
        $idPc = $this->db->insert_id();

        if ($in) {
            redirect('Project_List/scope?id_pc=' . $idPc);
        } else {
            redirect('Project_List/charter');
        }

    }

    # !OPTIONAL

    public function cekStatus($val = '')
    {
        // Definisi
        $cek = '';

        switch ($val) {
            case 1:
                $cek = "DRAFT";
                break;
            case 2:
                $cek = "ON PROGRESS";
                break;
            case 3:
                $cek = "DONE";
                break;
            case 4:
                $cek = "PENDING";
                break;
            default:
                $cek = "";
                break;
        }

        return $cek;
    }

    public function dtProjectList()
    {
        // Definisi
        $condition = '';
        $data = [];
        
        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'task', 'pic_sts', 'pic_tcel', 'status', 'note', 'review');
        // Set searchable column fields
        $CI->dt->column_search = array('task', 'pic_sts', 'pic_tcel', 'status', 'note', 'review');
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                // $i,
                '<input onclick="edit(' . $dt->id . ')" type="checkbox" name="id[]"
                                                value="' . $dt->id . '">',
                '<a href="' . site_url("Project_List/charter?id_pc=" . $dt->id) . '" > ' . $dt->task . '</a>',
                $dt->pic_sts,
                $dt->pic_tcel,
                $this->cekStatus($dt->status),
                $dt->note,
                $dt->review,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function dashboard($tahun = '')
    {
        if ($tahun == '') {
            $tahun = date('Y');
            $qTahunAll = 'where tanggal like "' . $tahun . '-%"';
            $qTahun = 'WHERE tanggal like "' . $tahun . '-%" AND ';
        } else if ($tahun == 'all') {
            $qTahun = 'WHERE';
            $qTahunAll = '';
        } else {
            $qTahunAll = 'WHERE tanggal like "' . $tahun . '-%"';
            $qTahun = 'WHERE tanggal like "' . $tahun . '-%" AND ';
        }

        // Definis
        $log = [];

        // Project List All Years
        $total_pl_year = $this->db->query("SELECT count(*) as jml FROM pl $qTahunAll ")->row()->jml;

        // Draft
        $draft = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 1")->row()->jml;

        // On Progress
        $onProgress = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 2")->row()->jml;

        // Done
        $done = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 3")->row()->jml;

        // Pending
        $pending = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 4")->row()->jml;

        $log = [
            'total' => $total_pl_year,
            'data' => [
                ['DONE (' . $done . ') ', (int) $done],
                ['PENDING (' . $pending . ') ', (int) $pending],
                ['ON PROGRESS (' . $onProgress . ')', (int) $onProgress],
                ['DRAFT (' . $draft . ')', (int) $draft],
            ],
        ];

        return $log;
    }

    public function insert_multiple($data = '')
    {
        if ($data != '') {
            $this->db->insert_batch('pl', $data);
        }

    }

}

/* End of file ProjectListModel.php */
/* Location: ./application/models/ProjectListModel.php */