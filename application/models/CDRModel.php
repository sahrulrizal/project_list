<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CDRModel extends CI_Model
{

    private $t = 'cdr';
    public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) {
            redirect('/');
        }
	}
    // ~ Costumer Daily Request

    public function getCDR($date = '')
    {
        $startDate = $this->input->get('startDate');
        $endDate = $this->input->get('endDate');

        // DATE KOSONG MASUK KE GET DATE
        if ($date == '') {
            $date = $this->input->get('date');
        }

        // KALAU DI GET DATE KOSONG MASUK KE DEFAULT DATE(Y-M-D)
        if ($date == '') {
            $arr = ['date(created_date)' => date('Y-m-d')];
        } else {
            $arr = ['date(created_date)' => $date];
        }

        // KALAU ADA INPUTAN STARTDATE DAN ENDDATE
        if ($startDate != '' && $endDate != '') {
            $q = $this->db->query("SELECT * FROM " . $this->t . " WHERE (created_date BETWEEN '" . $startDate . "' AND '" . $endDate . "')");
        } else {
            $this->db->order_by('id', 'desc');
            $q = $this->db->get_where($this->t, $arr);
        }

        return $q;
    }

    public function getCDRID($id = '')
    {
        if ($id == '') {
            $id = $this->input->get('id');
        }

        $q = $this->db->get_where($this->t, ['id' => $id]);
        return $q;
    }

    public function cekStatus($status = '')
    {
        if ($status == '') {
            $status = $this->input->get('status');
        }

        switch ($status) {
            case 1:
                $q = 'DRAFT';
                break;
            case 2:
                $q = 'ON PROGRESS';
                break;
            case 3:
                $q = 'DONE';
                break;
            case 4:
                $q = 'PANDING';
                break;
            default:
                $q = 'TIDAK DIKETEHUI';
                break;
        }

        return $q;
    }

    public function getCDRAll($id = '')
    {
        if ($id == '') {
            $id = $this->input->get('id');
        }

        $id = explode(',', $id);
        $this->db->where_in('id', $id);
        $q = $this->db->get($this->t);
        return $q;
    }

    public function inCDR($object = '')
    {

        if ($object == '') {

            $object = [
                'request_date' => $this->input->post('request_date'),
                'costumer_request' => $this->input->post('costumer_request'),
                'whom' => $this->input->post('whom'),
                'status' => $this->input->post('status'),
                'request_done' => $this->input->post('request_done'),
                'request_by' => $this->input->post('request_by'),
                'review' => $this->input->post('review'),
                'note' => $this->input->post('note'),
                'created_date' => date('Y-m-d H:i:s'),
            ];

        }

        $q = $this->db->insert($this->t, $object);

        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function upCDR($obj = '', $id = '', $based_on = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->t, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Daily Request ',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deCDR($id = '')
    {
        if ($id == '') {
            $id = $this->input->get('id');
        }

        if ($id != '') {
            $id = explode(',', $id);
            $this->db->where_in('id', $id);
            $q = $this->db->delete($this->t);

            if ($q) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    // datatable
    public function dtCdr()
    {
        // Definisi
        $condition = '';
        $data = [];

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->t;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'request_date', 'costumer_request', 'whom', 'status', 'request_done', 'request_by', 'note', 'review');
        // Set searchable column fields
        $CI->dt->column_search = array('request_date', 'costumer_request', 'whom', 'status', 'request_done', 'request_by', 'note', 'review');
        // Set select column fields
        $CI->dt->select = $this->t . '.*';
        // Set default order
        $CI->dt->order = array($this->t . '.id' => 'desc');

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(

                '<a href="#ok" onclick="edit(' . $dt->id . ')" >' . $dt->request_date . '</a>',
                $dt->costumer_request,
                $dt->whom,
                $this->cekStatus($dt->status) . '<br>' . $dt->request_done,
                $dt->request_by,
                $dt->note,
                $dt->review,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

}