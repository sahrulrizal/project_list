<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Project</li>
		<li>Input</li>
		<li>Project Charter</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Edit Statement</h2>
				</header>

				<!-- widget div-->
				<div>
					<form action="<?php echo base_url(). 'action/upStatement' ?>" method="post" onSubmit="validasi()">
						<?php 
						$date = date_default_timezone_set("Asia/Jakarta");
						$date = date("Y-m-d H-m-s");
						?>
						<?php $no = 1; foreach ($statement->getStatementAll()->result() as $v): ?>
							
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Activity</label>
									<input type="hidden" name="id[]" value="<?php echo $v->id ?>">
									<input type="hidden" name="inp" value="<?php echo $this->input->get('inp'); ?>">
									<input type="text" name="activity[]" value="<?=$v->activity;?>" class="form-control" id="tags" placeholder="Acitvity">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Mitra</label>
									<select class="form-control" name="mitra">
										<?php foreach ($mitra->getMitra()->result() as $m): ?>
											<option value="<?= $m->id; ?>"><?=$m->mitra;?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Start Target Date</label>
									<input type="date" name="start_target[]" value="<?=$v->start_target;?>" class="form-control" id="tags" placeholder="Mobile Number">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">End Target Date</label>
									<input type="date" name="end_target[]" value="<?=$v->end_target;?>" class="form-control" id="tags" placeholder="Mobile Number">
								</div>
							</div>
						</div>
						<hr>
						<?php endforeach ?>


						<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

						<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

						<button class="btn btn-labeled btn-primary"><span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</button>

					</div>
				</form>
				<script data-pace-options=' {
				"restartOnRequestAfter": true;
			}		
			' src="js/plugin/pace/pace.min.js"></script>
		</div>
	</div>
</section>
</div>
</div>
