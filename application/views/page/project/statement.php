<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Project</li>
		<li>Input</li>
		<li>Statement Of Work</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<?php $this->load->view('template/menu_add'); ?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Statement Of Work</h2>

				</header>

				<!-- widget div-->
				<div>
					<!-- widget content -->
					<div style="margin-bottom: 10px;">
						<a href="javascript:void(0);"  data-toggle="modal" data-target="#add" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span><span>Add</span></a>

						<a href="javascript:void(0);" id="linkEdit" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-minus-sign"></i></span><span>Edit</span></a>

						<a href="javascript:void(0);" onclick="return confirm('Anda yakin ingin menghapus statement ini ?')" id="linkDelete" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></span><span>Delete</span></a>
					</div>
					<div class="widget-body">

						<div class="tabbable">
							<ul class="nav nav-tabs  bordered">
								<li class="active">
									<a href="#tab1" data-toggle="tab" rel="tooltip" data-placement="top">Connectivity</a>
								</li>

								<li>
									<a href="#tab2" data-toggle="tab" rel="tooltip" data-placement="top">Non Connectivity</a>
								</li>
							</ul>
							<div class="tab-content">
									<form method="get" action="<?=site_url('action/inStatementList')?>">
										<input type="hidden" name="id_pc" value="<?=$this->input->get('id_pc');?>">
								<div class="tab-pane in active" id="tab1">
									<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
										<thead>			                
											<tr>
												<th style="width: 30px;">
													<form class="smart-form">
														
													</form>
												</th>
												<th>Activity</th>
												<th>Mitra</th>
												<th>Start Target Date</th>
												<th>End Target Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($statement->getStatement()->result() as $v): ?>

											<tr>
												<td>
													<div class="smart-form">
														<label class="checkbox">
															<input type="checkbox"  <?= $statement->cekStatus($v->id,$this->input->get('id_pc')) == true ? 'checked' : '' ?> id="cek" type="checkbox" name="id_statement[]" value="<?php echo $v->id ?>"><i></i>
														</label>
													</div>
												</td>
												<td><?=$v->activity;?></td>
												<td><?=$v->mitra;?></td>
												<td><?=$v->start_target;?></td>
												<td><?=$v->end_target;?></td>
											</tr>
											<?php endforeach ?>

										</tbody>
									</table>
								
								</div>
									<button style="margin: 10px;" type="submit" name="btn">Lanjut</button>
									</form>

								<div class="tab-pane" id="tab2">
								</div>
							</div>
						</div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
		</section>
	</div>
</div>
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							×
						</button>
						<h4 class="modal-title" id="myModalLabel">Add</h4>
					</div>
					
					<form method="post" action="<?=site_url('action/inStatement');?>" id="formAdd">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Activity</label>
									<input type="text" name="activity"  class="form-control" id="tags" placeholder="Acitvity">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Mitra</label>
									<select class="form-control" name="mitra">
										<?php foreach ($mitra->getMitra()->result() as $m): ?>
											<option value="<?= $m->id; ?>"><?=$m->mitra;?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Start Target Date</label>
									<input type="date" name="start_target" class="form-control" id="tags" placeholder="Start Target Date">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">End Target Date</label>
									<input type="date" name="end_target"  class="form-control" id="tags" placeholder="End Target Date">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
						</button>
						<button type="submit" class="btn btn-primary">
							Save
						</button>
					</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">

$('#cek').change(function(event) {
		event.preventDefault();

		var ID = [];
		$.each($("input[name='id[]']:checked"), function(){            
			ID.push($(this).val());
		});

		var id = ID.join(",");
		edit(id);
		hapus(id);
	});	


 function edit(id) {
   $("#linkEdit").attr("href", "<?php echo base_url(); ?>Project_list/editStatement?id="+id+"&inp="+<?=$this->input->get('id_pc')?>);
 }

 function hapus(id) {
   $("#linkDelete").attr("href", "<?php echo base_url(); ?>action/deStatement?id="+id);
 }

</script>
