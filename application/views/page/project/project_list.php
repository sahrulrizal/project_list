<div id="main" role="main">

    <?php $this->load->view('template/breadcumb');?>
    <style>
    .dataTables_filter {
        display: block;
    }

    .dataTables_length {
        display: block;
    }
    </style>
    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid">
            <!-- NEW WIDGET START -->
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2><?=$smallTitle;?></h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="row" style="margin-top: 10px; margin-left: 1px;">
                            <div class="col-md-12">
                                <a href="<?php echo base_url(); ?>Project_List/charter"
                                    class="btn btn-labeled btn-default"><span class="btn-label"><i
                                            class="glyphicon glyphicon-plus"></i></span><span>Add</span></a>

                                <a id="edit" class="btn btn-labeled btn-default"><span class="btn-label"><i
                                            class="fa fa-fw fa-pencil-square-o"></i></span><span>Edit</span></a>

                                <a class="btn btn-labeled btn-default" id="btn-delete"
                                    onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')"><span
                                        class="btn-label"><i
                                            class="glyphicon glyphicon-minus-sign"></i></span><span>Delete</span></a>
                                <a href="<?php echo base_url('Project_List/cetak_pdf'); ?>" target="_blank"
                                    class="btn btn-primary">Cetak Pdf</a>
                                <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#myModal">
                                    Import Excel
                                </button>
                            </div>
                        </div>
                        <form method="post" action="<?php echo base_url('query/delete') ?>" id="form-delete">
                            <table id="contoh" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Task</th>
                                        <th>PIC STS</th>
                                        <th>PIC TCEL</th>
                                        <th>Status</th>
                                        <th>Note</th>
                                        <th>Review</th>
                                    </tr>
                                    <!-- <tr>
                                        <td colspan="2"></td>
                                        <td>STS</td>
                                        <td>TCEL</td>
                                        <td colspan="2"></td>
                                    </tr> -->
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
                </form>
            </div>
        </section>
        <!-- modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import Excel</h4>
                    </div>
                    <form class="form-horizontal" id="submit" enctype=" multipart/form-data">
                        <div class="modal-body">
                            <div class="well">
                                <i class="fa-info-circle fa"></i> Information
                                <p>
                                    Please download template below before uploading<br>
                                    <a href="<?php echo base_url('excel/data.xlsx'); ?>" id="btnDownloadTemplate"
                                        class="btn btn-info">Download
                                        Template Excel</a>
                                </p>
                            </div>
                            <input type="file" name="file" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
    showTable();


    $("#btn-delete").click(function() { // Ketika user mengklik tombol delete
        if (confirm) // Jika user mengklik tombol "Ok"
            $("#form-delete").submit(); // Submit form
    });

});

function test(id) {
    var ID = [];
    $.each($("input[name='id[]']:checked"), function() {
        ID.push($(this).val());
    });

    var id = ID.join(",");
    edit(id);
    hapus(id);
}

function edit(id) {
    $("#edit").attr("href", "<?php echo base_url(); ?>Project_List/charter?id_pc=" + id);
}

function hapus(id) {
    $("#btn-delete").attr("href", "<?php echo base_url(); ?>Action/deletePc?id=" + id);
}

function showTable() {
    // body...
    $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("Project_List/getdtprojectlist");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{
            "targets": [0],
            "orderable": false
        }]
    });
}
$(document).ready(function() {

    $('#submit').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: '<?php echo base_url(); ?>Project_List/import',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            success: function(data) {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    // title: result.msg,
                    html: '<div style="font-size:16px;">' + data.msg + '</div>',
                    showConfirmButton: false,
                    timer: 2000
                });
                showTable();
                $('input[name=file]').val('');
            }
        });
    });


});
</script>