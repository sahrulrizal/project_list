<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Manage Service</li>
		<li>Closed</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid">
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>List Closed of Manage Service</h2>
				</header>

				<!-- widget div-->
				<div>
					<!-- widget content -->
					<div class="widget-body no-padding">
						<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
							<thead>			                
								<tr>
									<th>PIC Telkomcel</th>
									<th>PIC STS</th>
									<th>Segmen</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Tanggal Testing</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>test</td>
									<td>test</td>
									<td>test</td>
									<td>test</td>
									<td>test</td>
									<td>test</td>
									<td>test</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>