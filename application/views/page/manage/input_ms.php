<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Manage Service</li>
		<li>Input</li>
		<li>Service</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<div class="row">
				<div class="col-md-12">&nbsp;
					&nbsp;
					<a href="input_ms.php" class="btn btn-danger">Service</a>
					<a href="document_ms.php" class="btn btn-default">Document</a>
				</div>
			</div><br>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Service</h2>
				</header>

				<!-- widget div-->
				<div>
					<p>Nama Project : *</p>
					<section>
						<label class="textarea">
							<textarea rows="3" name="info" placeholder="" style="resize: none;
							width: 650px;
							height: 80px;
							"> </textarea>
						</label>
					</section>

					<div class="row">
						<div class="col-md-6">
							<br>
							<div class="form-group">
								<label>PIC TELKOMCEL: *</label>
								<input type="text" name="password" class="form-control" placeholder="Person In Charge Telkomcel">
							</div>

							<div class="form-group">
								<label>PIC STS: *</label>
								<input type="text" name="password" class="form-control" placeholder="Person In Charge STS">
							</div>

							<label>Segmen : *</label>
							<form id="checkout-form" class="smart-form" novalidate="novalidate">
								<label class="select">  
									<select name="country">
										<option value="0" selected="" disabled="">-- Pilihan --</option>
										<option value="1"> BSS </option>
										<option value="2"> VAS </option>
										<option value="3"> CORE </option>
									</select> <i></i> </label>
								</form><br>
								
								<div class="form-group">
									<label>Status: *</label>
									<form id="checkout-form" class="smart-form" novalidate="novalidate">
										<label class="select">  
											<select name="country">
												<option value="0" selected="" disabled="">-- Pilihan --</option>
												<option value="1"> On Going </option>
												<option value="2"> Closed </option>
											</select> <i></i>
										</label>
									</form>
								</div>
							</div>

							<div class="col-md-6">
								<br>


								<label for="nama"> Start Date : </label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" name="request" placeholder="Start Date" class="datepicker" data-dateformat='dd/mm/yy'>
									</label>

								</div><br>

								<label for="nama"> End Date : </label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" name="request" placeholder="End Date" class="datepicker" data-dateformat='dd/mm/yy'>
									</label>

								</div><br>

								<label for="nama"> Tanggal Testing : </label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" name="request" placeholder="Tanggal Testing" class="datepicker" data-dateformat='dd/mm/yy'>
									</label>

								</div><br>
							</div>
						</div>

						<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

						<a href="" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

						<a href="" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</a>

					</div>
					<script data-pace-options=' {
					"restartOnRequestAfter": true;
				}		
				' src="js/plugin/pace/pace.min.js"></script>
			</div>
		</div>
	</section>
</div>
</div>