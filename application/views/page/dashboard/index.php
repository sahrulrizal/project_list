<?php $status = array();?>
<style type="text/css">
.box {
    padding: 10px;
}

.total {
    font-size: 20px;
    color: #FFF;
}

.jumlah {
    font-size: 18px;
    color: #FFF;
}
</style>
<div id="main" role="main">
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Dashboard</li>
    </ol>
    <!-- MAIN CONTENT -->

    <div id="content">


        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-12">
                <div class="box" style="background: #555;">
                    <div class="row">
                        <div class="col-md-12 text-center">

                            <div class="total">Total List Project
                                <select name="tahun" onchange="chartTahun(this.value)" style="color: #00BCD4; background: transparent; border: 0;">
                                    <option value="all">Semua</option>
                                    <?php for ($i = 18; $i <= 30; $i++) {?>
                                        <option <?=date('Y') == ('20' . $i) ? 'selected' : ''?> value="<?='20' . $i;?>"><?='20' . $i;?></option>
                                     <?php }
;?>
                                </select><span id="total"></span></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div id="chartdiv"></div>

    </div>

    <!-- end row -->
    <!-- row -->
    <div class="row">

        <!-- a blank row to get started -->
        <div class="col-sm-12">
            <!-- your contents here -->
        </div>

    </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">

$(document).ready(function () {
    chart();
});

function chart(t='') {
    Highcharts.setOptions({
        colors: ['#0000e8', '#54b03d', '#fac100', '#da1438', '#f67716']
    });

    $.ajax({
    type: "GET",
    url: "<?=site_url('dashboard/dataDashboard?tahun=');?>"+t,
    dataType: "JSON",
        success: function(r) {
            var data = r.data;
            $('#total').text('(' + r.total + ')');

            Highcharts.chart('chartdiv', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    },
                    backgroundColor: 'rgba(255, 255, 255, 0.0)'
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b style="font-size:20px;"><{series.name}/b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        innerSize: 200,
                        depth: 45,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}',
                            style: {
                                fontFamily: '\'Lato\', sans-serif',
                                lineHeight: '16px',
                                fontSize: '16px'
                            }
                        }

                    }
                },
                series: [{
                    type: 'pie',
                    data: data,
                }]
            });
        }
    });
}

function chartTahun(v) {
    chart(v);
}

</script>