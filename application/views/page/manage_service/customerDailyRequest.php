<div id="main" role="main">

    <?php $this->load->view('template/breadcumb');?>


    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid">
            <!-- NEW WIDGET START -->
            <!-- Widget ID (each widget will need unique ID)-->
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                          -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <span id="txtAction" style="
                            font-size: 14px;
                            font-weight: bold;
                            position: relative;
                            top: -5px;
                            left: 5px;
                            ">Add </span>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding" id="ok">

                        <form id="inForm" method="POST" action="javascript:void(0);" class="smart-form"
                            novalidate="novalidate">

                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <span id="cloneID"></span>
                                        <label>Request Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="date" name="request_date" id="startdate"
                                                placeholder="Request Date">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Costumer Request</label>
                                        <label class="input"> <i class="icon-append fa fa-pencil-square-o"></i>
                                            <input type="text" name="costumer_request" placeholder="Costumer Request">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Whom</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="whom" placeholder="Whom">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Request Done</label>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="date" name="request_done" placeholder="Date">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Request By</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="request_by" placeholder="Request By">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Review</label>
                                        <label class="input"> <i class="icon-append fa fa-search"></i>
                                            <input type="text" name="review" placeholder="Review">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Note</label>
                                        <label class="textarea">
                                            <textarea rows="3" name="note" placeholder="Note"></textarea>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Status: *</label>
                                        <div id="checkout-form" class="smart-form" novalidate="novalidate">
                                            <label class="select">
                                                <select name="status" id="status">
                                                    <option value="" disabled="">-- Pilihan --</option>
                                                    <option value="1" selected=""> DRAFT </option>
                                                    <option value="2"> ON PROGRESS </option>
                                                    <option value="3"> DONE </option>
                                                    <option value="4"> PENDING </option>
                                                </select> <i></i>
                                            </label>
                                        </div>
                                    </section>

                                </div>

                            </fieldset>

                            <footer>
                                <button type="submit" class="btn btn-primary" id="txtAction" onclick="proses()">
                                    Add
                                </button>
                                <button type="reset" class="btn btn-default" onclick="ubahText('','add')">
                                    Cancel
                                </button>
                            </footer>
                        </form>


                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

            <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2><?=$smallTitle;?></h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <form method="post" action="<?php echo base_url('query/delete') ?>" id="form-delete">
                            <table id="contoh" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Request Date</th>
                                        <th>Costumer Request</th>
                                        <th>Whom</th>
                                        <th>Status & Date</th>
                                        <th>Request By</th>
                                        <th>Note</th>
                                        <th>Review</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
$no = 1;
foreach ($model->getCDR(date('Y-m-d'))->result() as $row) {
    ?>
                                    <tr>
                                        <td><a href="#ok"
                                                onclick="edit(<?=$row->id;?>)"><?php echo $row->request_date; ?></a>
                                        </td>
                                        <td><?php echo $row->costumer_request; ?></td>
                                        <td><?php echo $row->whom; ?></td>
                                        <td><?php echo $model->cekStatus($row->status); ?><br><?=$row->request_done;?>
                                        </td>
                                        <td><?php echo $row->request_by; ?></td>
                                        <td><?php echo $row->note; ?></td>
                                        <td><?php echo $row->review; ?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                    </div>
                </div>
                </form>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    showTable();
    // prosesInCDR();
    // prosesUpCDR();

});

function proses() {
    var id = $('input[name=id]').val();
    if (id != undefined) {
        prosesUpCDR(id);
    } else {
        prosesInCDR();
    }
}

function prosesInCDR() {
    event.preventDefault();
    $.ajax({
            url: '<?=base_url("ManageService/inCDR");?>',
            type: 'POST',
            dataType: 'JSON',
            data: $('form').serialize()
        })
        .done(function(data) {

            Swal.fire(
                'Sukses!',
                data.msg,
                'success'
            );
            showTable();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function prosesUpCDR() {
    event.preventDefault();
    $.ajax({
            url: '<?=base_url("ManageService/upCDR");?>',
            type: 'POST',
            dataType: 'JSON',
            data: $('form').serialize()
        })
        .done(function(data) {

            Swal.fire(
                'Sukses!',
                data.msg,
                'success'
            );
            showTable();

        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function edit(id = '') {
    ubahText(id, 'edit');
    if (id != '') {

        $.ajax({
                url: '<?=base_url("ManageService/getCDRID?id=");?>' + id,
                type: 'GET',
                dataType: 'JSON'
            })
            .done(function(data) {
                $('input[name=request_date]').val(data.request_date);
                $('input[name=costumer_request]').val(data.costumer_request);
                $('input[name=whom]').val(data.whom);
                $('input[name=request_done]').val(data.request_done);
                $('input[name=request_by]').val(data.request_by);
                $('input[name=review]').val(data.review);
                $('textarea[name=note]').text(data.note);
                $('select[name=status]').val(data.status);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

    }
}

function ubahText(id = '', val = '') {

    var r;
    if (val == 'add') {
        r = 'Add';
        $('#txtAction*').text(r);
        $('#upForm').attr('id', '');
        $('#cloneID').html('');
        $('textarea[name=note]').html('');

    } else if (val == 'edit') {
        r = 'Edit';
        $('#txtAction*').text(r);
        $('#inForm').attr('id', '');
        $('#cloneID').html("<input type='hidden' name='id' value='" + id + "'>");
    }

    return r;
}


function showTable() {
    // body...
    $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        // "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("ManageService/dtCdr");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{
            "targets": [2],
            "visible": true,
            "searching": true
        }, {
            "targets": [3],
            "visible": true
        }]
    });


}
</script>