<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Project</li>
		<li>Input</li>
		<li>Project Charter</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->

		<?php $this->load->view('template/menu_add');?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Project Charter</h2>
				</header>

				<!-- widget div-->
				<div>
					<form action="<?php echo base_url().$action ?>" method="<?=$method;?>" onSubmit="validasi()">
						<?php 
						$date = date_default_timezone_set("Asia/Jakarta");
						$date = date("Y-m-d H-m-s");
						?>

						<div class="form-group" style=" margin-bottom: 0;">
							<label>Task : *</label>
							<input type="text" required name="task" class="form-control" id="task" placeholder="Task" value="">
						</div>

						<div class="form-group" style=" margin-top: 10px;">
							<label><b>STATUS : </b></label>
							<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered" border="1" style="width: 100%">
									<tr>
										<td>STS</td>
										<td>TCEL</td>
									</tr>
									<tr>
										<td>
											<select class="form-control" name="s_sts" id="s_sts" style="width: 100%;">
												<option value="" selected="" disabled=""></option>
												<option value="1"> DRAFT </option>
												<option value="2"> ON PROGRESS </option>
												<option value="3"> DONE </option>
												<option value="4"> PANDING </option>
											</select>
										</td>
										<td>
											<select class="form-control" name="s_tcel" id="s_tcel" style="width: 100%;">
											<option value="" selected="" disabled=""></option>
											<option value="1"> DRAFT </option>
											<option value="2"> ON PROGRESS </option>
											<option value="3"> DONE </option>
											<option value="4"> PANDING </option>
										</select>
										</td>
									</tr>
									<tr>
										<td>
											<input type="text" class="form-control" name="ket_s_sts" id="ket_s_sts" style="width: 100%;" placeholder="Keterangan STS">
										</td>
										<td>
											<input type="text" class="form-control" name="ket_s_tcel" id="ket_s_tcel" style="width: 100%;" placeholder="Keterangan TCEL">
										</td>
									</tr>
								</table>
							</div>
						</div>

						<div class="form-group" >
							<label><b>PIC : </b></label>
							<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered" border="1" style="width: 100%">
									<tr>
										<td>STS</td>
										<td>TCEL</td>
									</tr>
									<tr>
										<td>
											<input type="text" class="form-control" name="pic_sts" id="pic_sts" style="width: 100%;" placeholder="PIC STS">
										</td>
										<td>
											<input type="text" class="form-control" name="pic_tcel" id="pic_tcel" style="width: 100%;" placeholder="PIC TCEL">
										</td>
									</tr>
								</table>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Confirm By: *</label>
									<input type="text" required name="confirm" id="confrim" class="form-control" placeholder="Confrim By ">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Request Via : *</label>
									<input type="text" required name="via" id="requestVa" class="form-control" placeholder="Request Via ">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Note : </label>
									<textarea name="note" id="note" class="form-control" placeholder="Note "></textarea>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Review : </label>
									<textarea name="review" id="review" class="form-control" placeholder="Review "></textarea>
								</div>
							</div>
						</div>

						<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

						<a href="<?php echo base_url(); ?>Project_list/project_list" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

						<button class="btn btn-labeled btn-primary"><span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</button>

					</div>
				</form>
				<script data-pace-options=' {
				"restartOnRequestAfter": true;
			}		
			' src="js/plugin/pace/pace.min.js"></script>
		</div>
	</div>
</section>
</div>
</div>