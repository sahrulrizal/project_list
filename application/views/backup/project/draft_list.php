<div id="main" role="main">
 
 <?php $this->load->view('template/breadcumb');?>

 <div id="content">
  <!-- widget grid -->
  <section id="widget-grid">
   <!-- NEW WIDGET START -->
   <!-- Widget ID (each widget will need unique ID)-->
   <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
    <header>
     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
     <h2><?=$smallTitle;?></h2>
    </header>

    <!-- widget div-->
     <div>
      <!-- widget content -->
      <div class="widget-body no-padding">
       <div class="row" style="margin-top: 10px; margin-left: 1px;">
        <div class="col-md-12">
         <a href="<?php echo base_url(); ?>Project_list/charter" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span><span>Add</span></a>

         <a id="edit" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-fw fa-pencil-square-o"></i></span><span>Edit</span></a>

         <a class="btn btn-labeled btn-default" id="btn-delete" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')"><span class="btn-label"><i class="glyphicon glyphicon-minus-sign"></i></span><span>Delete</span></a>
        </div>
       </div>
       <form method="post" action="<?php echo base_url('query/delete') ?>" id="form-delete">
       <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%" style="display: block;
        overflow-x: auto;
        white-space: nowrap;" >
        <thead>                   
         <tr>
          <th></th>
          <th>Nomor Dinas</th>
          <th>Tanggal Nomor Dinas</th>
          <th>PIC STS</th>
          <th>PIC Telkomcel</th>
          <th>Request Department</th>
          <th>Request Date</th>
          <th>End Date</th>
          <th>Testing Date</th>
          <th>Release Date</th>
          <th>Note</th>
          <th>Confirm</th>
          <th>Via</th>
          <th>Status</th>
         </tr>
        </thead>
        <tbody>
         <?php 
         foreach ($project->se(1)->result() as $row) {
          ?>
          <tr>
           <td>
            <input onchange="test(this)" type="checkbox" name="id[]" value="<?php echo $row->id ?>">
           </td>
           <td><?php echo $row->no ?></td>
           <td><?php echo $row->tanggal_nodin; ?></td>
           <td><?php echo $row->pic_sts; ?></td>
           <td><?php echo $row->pic_telkomcel; ?></td>
           <td><?php echo $row->segmen; ?></td>
           <td><?php echo $row->start_date; ?></td>
           <td><?php echo $row->end_date; ?></td>
           <td><?php echo $row->tanggal_testing; ?></td>
           <td><?php echo $row->tanggal_rilis; ?></td>
           <td><?php echo $row->note; ?></td>
           <td><?php echo $row->confirm; ?></td>
           <td><?php echo $row->via; ?></td>
           <td><?php echo $project->cekStatus($row->status); ?></td>
          </tr>
         <?php } ?>
        </tbody>
       </table>
      </div>
     </div>
    </form>
   </div>
  </section>
 </div>
</div>
<script src="<?php echo base_url('assets/jquery.min.js'); ?>"></script>
<script type="text/javascript">
 $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
     


  $("#btn-delete").click(function(){ // Ketika user mengklik tombol delete
      var confirm = window.confirm("Apakah Anda yakin ingin menghapus data-data ini?"); // Buat sebuah alert konfirmasi
      
      if(confirm) // Jika user mengklik tombol "Ok"
        $("#form-delete").submit(); // Submit form
    });

  });

 function test(id) {
  var ID = [];
  $.each($("input[name='id[]']:checked"), function(){            
   ID.push($(this).val());
  });

  var id = ID.join(",");
  edit(id);
  hapus(id);
 }

 function edit(id) {
   $("#edit").attr("href", "<?php echo base_url(); ?>Project_list/viewUpPc?id="+id);
 }

 function hapus(id) {
   $("#btn-delete").attr("href", "<?php echo base_url(); ?>action/deletePc?id="+id);
 }
 
</script>
