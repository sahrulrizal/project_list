<style>
#customers {
  font-family: "Times New Roman", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}

#customers tbody,thead{
    font-size: 14px;
}
</style>

<h2 style="text-align: center">PROJECT LIST</h2>
<table id="customers"  width="100%">
    <thead>
    <tr>
    <th>No</th>
    <th>Task</th>
    <th>PIC STS</th>
    <th>PIC TCEL</th>
    <th>Status</th>
    <th>Note</th>
    <th>Review</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($project->se($status)->result() as $row) {
            ?>
            <tr>
            <td>
            <?=$no++;?> 
            </td>
            <td><a href="<?=site_url('Project_List/charter?id_pc=' . $row->id)?>"
            target='_blank'><?php echo $row->task ?></a></td>
            <td><?php echo $row->pic_sts; ?></td>
            <td><?php echo $row->pic_tcel; ?></td>
            <td><?php echo $project->cekStatus($row->status); ?></td>
            <td><?php echo $row->note; ?></td>
            <td><?php echo $row->review; ?></td>
            </tr>
            <?php }?>
    </tbody>
    </table>