<!DOCTYPE html>
<html lang="en-us">
<meta http-equiv="content-type" content="text/html;
charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
	<meta charset="utf-8">
	<title> <?=$title;?></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Basic Styles -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url() ?>template/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url() ?>template/css/font-awesome.min.css">

	<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url() ?>template/css/smartadmin-production-plugins.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url() ?>template/css/smartadmin-production.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url() ?>template/css/smartadmin-skins.min.css">

	<!-- SmartAdmin RTL Support -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url() ?>template/css/smartadmin-rtl.min.css">

	<!-- We recommend you use "your_style.css" to override SmartAdmin specific styles this will also ensure you retrain your customization with each SmartAdmin update. <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->
	
	<!-- #FAVICONS -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>template/img/favicon/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url() ?>template/img/favicon/favicon.ico" type="image/x-icon">

	<!-- #GOOGLE FONT -->
	<link rel="stylesheet" href="http: //fonts.googleapis.com/css?family=Open+Sans:400italic, 700italic, 300, 400, 700">

	<!-- #APP SCREEN / ICONS -->
	<!-- Specifying a Webpage Icon for Web Clip Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
	<link rel="apple-touch-icon" href="<?php echo base_url() ?>template/img/splash/sptouch-icon-iphone.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>template/img/splash/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>template/img/splash/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>template/img/splash/touch-icon-ipad-retina.png">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.css"/>
	<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<!-- Startup image for web apps -->
	<link rel="apple-touch-startup-image" href="<?php echo base_url() ?>template/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
	<link rel="apple-touch-startup-image" href="<?php echo base_url() ?>template/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
	<link rel="apple-touch-startup-image" href="<?php echo base_url() ?>template/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
	
	<style type="text/css">
	#contoh_wrapper{
		margin-top:10px;
	}
	.dataTables_filter {
		width : 300px;
	}
	
	</style>
	<link rel="stylesheet" href="<?=base_url('template/sweetalert2/')?>sweetalert2.min.css">
	
<!--Css DonutChart-->

<style type="text/css">
body {
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}
#chartdiv {
	width: 100%;
	height: 500px;
}
</style>
<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
</head>
<body>
	
	<?php echo $header; ?>
	<?php echo $content; ?>
	<?php echo $footer; ?>

	<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>template/js/plugin/pace/pace.min.js"></script>

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script>
		if (!window.jQuery) {
			document.write('<script src="<?php echo base_url(); ?>template/js/libs/jquery-2.1.1.min.js"><\/script>');
		}
	</script>

	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script>
		if (!window.jQuery.ui) {
			document.write('<script src="<?php echo base_url(); ?>template/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="<?php echo base_url(); ?>template/js/app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
	<script src="<?php echo base_url(); ?>template/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

	<!-- BOOTSTRAP JS -->
	<script src="<?php echo base_url(); ?>template/js/bootstrap/bootstrap.min.js"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="<?php echo base_url(); ?>template/js/notification/SmartNotification.min.js"></script>

	<!-- JARVIS WIDGETS -->
	<script src="<?php echo base_url(); ?>template/js/smartwidgets/jarvis.widget.min.js"></script>

	<!-- EASY PIE CHARTS -->
	<script src="<?php echo base_url(); ?>template/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	<!-- SPARKLINES -->
	<script src="<?php echo base_url(); ?>template/js/plugin/sparkline/jquery.sparkline.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script src="<?php echo base_url(); ?>template/js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script src="<?php echo base_url(); ?>template/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!-- JQUERY SELECT2 INPUT -->
	<script src="<?php echo base_url(); ?>template/js/plugin/select2/select2.min.js"></script>

	<!-- JQUERY UI + Bootstrap Slider -->
	<script src="<?php echo base_url(); ?>template/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	<!-- browser msie issue fix -->
	<script src="<?php echo base_url(); ?>template/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	<!-- FastClick: For mobile devices -->
	<script src="<?php echo base_url(); ?>template/js/plugin/fastclick/fastclick.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
	<script src="<?=base_url('template/sweetalert2/')?>sweetalert2.min.js"></script>

		
	<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

	<!-- MAIN APP JS FILE -->
	<script src="<?php echo base_url(); ?>template/js/app.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	
	<script>
		
		$(document).ready(function () {
			if(localStorage.getItem('id') == null || localStorage.getItem('id') == '' || localStorage.getItem('id') == 'null' ){
				window.location.href = "<?=site_url('/')?>";
			}			
		});

	</script>
	
</body>
</html>