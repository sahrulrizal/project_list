<?php

$id_charter = '';
$id = $this->input->get('id_pc');

if ($id != '') {
    $id = "?id_pc=" . $this->input->get('id_pc');
    $pc = 'charter';
} else if ($this->input->get('id') != '') {
    $id = "?id_pc=" . $this->input->get('id');
    $pc = 'viewUpPc';
} else {
    $id = '';
    $pc = 'charter';
}

if ($this->uri->segment(2) == 'mitra') {
    $view = 'viewUpPc';
    $id_charter = '?id_pc=' . $this->input->get('id_pc');
} else if ($this->uri->segment(2) == 'scope') {
    $view = 'viewUpPc';
    $id_charter = '?id_pc=' . $this->input->get('id_pc');
} else if ($this->uri->segment(2) == 'viewUpPc') {
    $view = 'viewUpPc';
    $id_charter = '?id_pc=' . $this->input->get('id');
}

?>

<div class="row">
	<div class="col-md-12">
		<a href="<?=site_url('Project_List/' . $pc . $id_charter)?>"  class="btn btn-<?=$this->uri->segment(2) == 'charter' ? "danger" : "default"?>">Project Charter</a>
		<!-- <a href="<?=site_url('Project_List/mitra' . $id)?>" class="btn btn-<?=$this->uri->segment(2) == 'mitra' ? "danger" : "default"?>">Mitra Project</a> -->
		<a href="<?=site_url('Project_List/scope' . $id)?>" class="btn btn-<?=$this->uri->segment(2) == 'scope' ? "danger" : "default"?>">Scope Of Work</a>
		<!-- <a href="<?=site_url('Project_List/document' . $id)?>" class="btn btn-default">Document</a> -->
	</div>
</div><br>