<header id="header">
	<div id="logo-group">
		<!-- PLACE YOUR LOGO HERE -->
		<span> <img src="<?php echo base_url() ?>template/img/telkomcel.png" alt="SmartAdmin"> </span>
		<!-- END LOGO PLACEHOLDER -->
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right">
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
			<span> <a href="javascript:void(0);" title="Collapse Menu" data-action="toggleMenu"><i class="fa fa-reorder"></i></a>
			</span>
		</div>
		<!-- end collapse menu -->
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
			<span> <a href="<?php echo base_url('login/signOut'); ?>" title="Sign Out" data-action="userLogout" data-logout-msg="Apakah anda yakin ingin logout ? "><i class="fa fa-sign-out"></i> Logout</a>
			</span>
		</div>
		<!-- end logout button -->
	</div>
	<!-- end pulled right: nav area -->
</header>

<aside id="left-panel">
	<nav>
		<ul>
			<li <?=$this->uri->segment(1) == 'dashboard' ? 'class="active"' : '';?>>
				<a href="<?php echo base_url(); ?>dashboard/" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i><span class="menu-item-parent">Dashboard</span></a>
			</li>

			<li <?=$this->uri->segment(1) == 'project_list' ? 'class="open active"' : '';?>>
				<a href="#" title="Project"><i class="fa fa-lg fa-fw fa-folder-open-o"></i> <span class="menu-item-parent">On Going Project </span></a>

				<ul <?=$this->uri->segment(1) == 'project_list' ? 'style="display: block"' : '';?>>
					<li <?=$this->uri->segment(2) == 'project_list' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>Project_List/project_list" title="Project List"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Project List</span></a>
					</li>
					<!-- <li <?=$this->uri->segment(2) == 'draft_list' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>Project_List/draft_list" title="Draft List"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Draft List</span></a>
					</li>
					<li <?=$this->uri->segment(2) == 'on_progress_list' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>Project_List/on_progress_list" title="On Progress List"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">On Progress List</span></a>
					</li>
					<li <?=$this->uri->segment(2) == 'done_list' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>Project_List/done_list" title="Done List"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Done List</span></a>
					</li>
					<li <?=$this->uri->segment(2) == 'panding_list' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>Project_List/panding_list" title="Panding List"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Panding List</span></a>
					</li> -->
				</ul>
			</li>

			<li <?=$this->uri->segment(1) == 'ManageService' ? 'class="open active"' : '';?>>
				<a href="#" title="Manage Service"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Service</span></a>
				<ul <?=$this->uri->segment(1) == 'ManageService' ? 'style="display: block"' : '';?>>
					<li <?=$this->uri->segment(2) == 'customerDailyRequest' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>ManageService/customerDailyRequest" title="Input"><span class="menu-item-parent">Costumer Daily Request</span></a>
					</li>
					<li <?=$this->uri->segment(2) == 'services' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>ManageService/services" title="On Going"> <span class="menu-item-parent">List Services STS</span></a>
					</li>
					<li <?=$this->uri->segment(2) == 'logDailyActivity' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>ManageService/logDailyActivity" title="On Going"> <span class="menu-item-parent">Log Daily Activity</span></a>
					</li>
					<!-- <li <?=$this->uri->segment(2) == 'dailyMonitor' ? 'class="active" ' : '';?>>
							<a href="<?php echo base_url(); ?>ManageService/dailyMonitor" title="Closed"> <span class="menu-item-parent">Daily Server Statistic & Disk usage Monitoring</span></a>
						</li> -->
					<li <?=$this->uri->segment(2) == 'dailyDevice' ? 'class="active" ' : '';?>>
						<a href="<?php echo base_url(); ?>ManageService/dailyDevice" title="Closed"> <span class="menu-item-parent">Log Daily Device</span></a>
					</li>
				</ul>
			</li>


			<!-- <li class="">
					<a href="#" title="Manage Profile"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Profile</span></a>
					<ul>
						<li>
							<a href="" title="Ganti Password"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Ganti Password</span></a>
						</li>

						<li class="">
							<a href="manage_user.php" title="Manage User"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage User</span></a>
						</li>
					</ul>
				</li> -->
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>
<!-- END NAVIGATION -->