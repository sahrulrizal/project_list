<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function render_page($content, $data=null)
	{
		$data['header']	= $this->load->view('template/header', $data, TRUE);	
		$data['content'] = $this->load->view($content, $data, TRUE);
		$data['footer']	= $this->load->view('template/footer', $data, TRUE);
		$this->load->view('template/index', $data);
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */