<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model('ModelLogin', 'ModelLogin');
        // $this->msg = "Sorry, this site is Under Maintenance";

    }

    public function index()
    {
        if ($this->session->userdata('id')) {
            redirect('dashboard');
        }
        $this->load->view('login/index');

    }
    public function check_login()
    {
        $log = [];
        $data['username'] = htmlspecialchars($_POST['username']);
        $data['password'] = htmlspecialchars($_POST['password']);
        $user = $this->ModelLogin->islogin($data);

        if ($user) {

            $data = array(
                'id' => $user['id'],
                'username' => $user['username'],
                'level' => $user['level'],
            );

            $this->session->set_userdata($data);

            $log = [
                'status' => true,
                'url' => site_url('dashboard/'),
                'session' => $data,
            ];
        } else {
            $log = [
                'status' => false,
                'msg' => 'Tidak dapat login, harap periksa kembali username dan password anda',
            ];
        }

        echo json_encode($log);
    }

    public function signOut()
    {
        $this->session->sess_destroy();
        // $this->load->view('login/index');
        echo "
        <script>
        localStorage.clear();
        window.location.href = '" . site_url('/') . "';
        </script>
        ";
    }
}
