<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project_List extends MY_Controller
{
    private $filename = "data"; // Kita tentukan nama filenya
    private $id = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_pc');
        $this->load->model('ProjectListModel', 'plm');
        $this->load->model('MitraModel', 'mm');
        $this->load->model('StatementModel', 'sm');
        $this->load->model('DocumentModel', 'dm');
        // $this->load->library('Cetak');

        // Daftarin id
        $this->id = $this->session->userdata('id');
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function index()
    {
        $data = [
            'report' => $this->M_pc->get_data_project(),
        ];
        // echo json_encode($data['report']);
        // echo $this->session->userdata('id');
        // $this->render_page('page/dashboard/index', $data);
    }

    public function cetakpdf()
    {
        // require_once APPPATH . 'third_party/wordwrap/wordwrap.php';
        $pdf = new PDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 12);
        // $text = str_repeat('this is a word wrap test ', 20);
        // $nb = $pdf->WordWrap($text, 120);
        // $pdf->Write(5, "This paragraph has $nb lines:\n\n");
        // $pdf->Write(5, $text);
        // $pdf->Output();
        // $pdf->SetFont('Arial', 'B', 16);
        // // mencetak string
        // $pdf->Cell(260, 7, 'Data Project List', 0, 1, 'C');
        // $pdf->SetFont('Arial', 'B', 22);
        // $pdf->Cell(190, 7, 'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK', 0, 1, 'C');
        // // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(20, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(40, 6, 'Task', 1, 0);
        $pdf->Cell(46, 6, 'Pic STS', 1, 0);
        $pdf->Cell(46, 6, 'Pic Tcel', 1, 0);
        $pdf->Cell(46, 6, 'Status', 1, 0);
        $pdf->Cell(46, 6, 'Note', 1, 0);
        $pdf->Cell(46, 6, 'Review', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $mahasiswa = $this->db->get('pl')->result();
        foreach ($mahasiswa as $row) {
            $nb = $pdf->WordWrap($row->task, 120);
            $pdf->Cell(40, 6, $nb, 1, 0);
            $pdf->Cell(46, 6, $row->pic_sts, 1, 0);
            $pdf->Cell(46, 6, $row->pic_tcel, 1, 0);

            $pdf->Cell(46, 6, $this->plm->cekStatus($row->status), 1, 0);
            $pdf->Cell(46, 6, $row->note, 1, 0);
            $pdf->Cell(46, 6, $row->review, 1, 1);
        }
        $pdf->Output();

    }

    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/project_list/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    array_push($data, array(
                        'task' => $row['A'], // Insert data nis dari kolom A di excel
                        'pic_sts' => $row['B'], // Insert data nis dari kolom A di excel
                        'pic_tcel' => $row['C'], // Insert data nis dari kolom A di excel
                        'status' => $row['D'], // Insert data nis dari kolom A di excel
                        'confirm' => $row['E'], // Insert data nis dari kolom A di excel
                        'via' => $row['F'], // Insert data nis dari kolom A di excel
                        'note' => $row['G'], // Insert data nis dari kolom A di excel
                        'review' => $row['H'], // Insert data nis dari kolom A di excel
                        'tanggal' => date('Y-m-d H:i:s'), // Insert data nis dari kolom A di excel
                        'scope' => $row['I'], // Insert data nis dari kolom A di excel

                    ));

                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // echo json_encode($data);
            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            $this->plm->insert_multiple($data);
        }
        $log = [
            'status' => true,
            'msg' => "Berhasil Import",
        ];
        echo json_encode($log);
        // redirect("Project_List/project_list"); // Redirect ke halaman awal (ke controller siswa fungsi index)
    }

    public function cetak_pdf(){

        $data = [
            'title' => 'Project List :: Project List All',
            'smallTitle' => 'Project List',
            'project' => $this->plm,
            'status' => ''
        ];

        $this->load->view('welcome_message',$data);
        
        // Get output html
        $html = $this->output->get_output();
        
        // Load pdf library
        $this->load->library('pdfs');
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');
        
        // Render the HTML as PDF
        $this->dompdf->render();
        
        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("project_list".date('Ymd').".pdf", array("Attachment"=>0));    
    }

    public function charter()
    {
        // Definisi
        $action = 'action/addPc';
        $method = 'post';

        $edit = [
            'id' => '',
            'task' => '',
            // 's_sts' => '',
            // 's_tcel' => '',
            // 'ket_s_sts' => '',
            // 'ket_s_tcel' => '',
            'status' => '',
            'pic_sts' => '',
            'pic_tcel' => '',
            'note' => '',
            'review' => '',
            'confirm' => '',
            'via' => '',
            'tanggal' => '',
        ];

        $id_pc = $this->input->get('id_pc');
        if ($id_pc != '') {

            $action = 'action/updatePc';
            $method = 'get';

            $pc = $this->M_pc->getProjectListID($id_pc)->row();

            $edit = [
                'id' => $pc->id,
                'task' => $pc->task,
                // 's_sts' => $pc->s_sts,
                // 's_tcel' => $pc->s_tcel,
                // 'ket_s_sts' => $pc->ket_s_sts,
                // 'ket_s_tcel' => $pc->ket_s_tcel,
                'status' => $pc->status,
                'pic_sts' => $pc->pic_sts,
                'pic_tcel' => $pc->pic_tcel,
                'note' => $pc->note,
                'review' => $pc->review,
                'confirm' => $pc->confirm,
                'via' => $pc->via,
                'tanggal' => $pc->tanggal,
            ];

        }

        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/project_list/charter', 'Project Charter')],
            'smallTitle' => 'Project Charter',
            'title' => 'Project Charter',
            'pl' => $edit,
            'action' => $action,
            'method' => $method,
        ];

        $this->render_page('page/project/charter', $data);
    }

    public function mitra()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/project_list/mitra', 'Mitra')],
            'mitra' => $this->mm,
            'title' => 'Input Mitra',

        ];

        $this->render_page('page/project/mitra', $data);
    }

    public function statement()
    {
        $data = [
            'statement' => $this->sm,
            'mitra' => $this->mm,

        ];
        $this->render_page('page/project/statement', $data);
    }

    public function document()
    {

        $data = [
            'document' => $this->dm,
        ];
        $this->render_page('page/project/document', $data);
    }

    // PROJECT CHARTER

    public function viewUpPc()
    {

        $id = $this->input->get('id');

        if ($id != '') {

            $data['segmen'] = $this->M_pc->segmen()->result();
            $data['data'] = $this->M_pc->get_by_id($id);
            $this->render_page('page/project/charter', $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function viewUpPd()
    {

        $id = $this->input->get('id');

        if ($id != '') {

            $data['segmen'] = $this->M_pc->segmen()->result();
            $data['data'] = $this->M_pc->get_by_id($id);
            $this->render_page('page/project/charter', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    // Mitra

    public function editMitra()
    {

        $id = $this->input->get('id');

        if ($id != '') {
            $data = [
                'mitra' => $this->mm,
            ];
            $this->render_page('page/mitra/formEdit', $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    // Statement

    public function editStatement()
    {

        $id = $this->input->get('id');

        if ($id != '') {
            $data = [
                'statement' => $this->sm,
                'mitra' => $this->mm,
            ];
            $this->render_page('page/statement/formEdit', $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    ########################################## NEW REVOLUTION #########################################

    # ~SCOPE

    public function scope()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/project_list/scope', 'Scope')],
            'smallTitle' => 'Scope Of Work',
            'title' => 'Scope Of Work',
            'project' => $this->M_pc,
        ];

        $this->render_page('page/project/scope', $data);
    }

    public function upScope()
    {
        $this->M_pc->upScope();
        redirect('Project_List/project_list');
    }

    # ~PROJECT LIST
    public function project_list()
    {

        $data = [
            'title' => 'Project List :: Project List All',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List')],
            'smallTitle' => 'Project List',
            'project' => $this->plm,
            'status' => '',
        ];

        $this->render_page('page/project/project_list', $data);
    }

    public function cetak()
    {
        $data = [
            'title' => 'Project List :: Project List All',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List')],
            'smallTitle' => 'Project List',
            'project' => $this->plm,
            'status' => '',
        ];

        // require_once('./html2pdf/html2pdf.class.php');
        // $pdf = new HTML2PDF('l','A5','en');
        // $html = $this->load->view('test', $data,true);

        // $pdf->WriteHTML($html);
        // $pdf->Output('Data Siswa.pdf', 'D');

        $this->load->view('test', $data);
    }

    //datatable
    public function getdtprojectlist()
    {
        echo $this->plm->dtProjectList();
    }

    # ~DRAFT_LIST

    public function draft_list()
    {

        $data = [
            'title' => 'Project List :: Draft List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/project_list/draft_list', 'Draft List')],
            'smallTitle' => 'Draft',
            'project' => $this->plm,
            'status' => 1,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    # ~ON PROGRESS

    public function on_progress_list()
    {
        $data = [
            'title' => 'Project List :: On Progress List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/project_list/on_progress_list', 'On Progress List')],
            'smallTitle' => 'On Progress List',
            'project' => $this->plm,
            'status' => 2,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    # ~DONE

    public function done_list()
    {
        $data = [
            'title' => 'Project List :: Done List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/project_list/done_list', 'Done List')],
            'smallTitle' => 'Done List',
            'project' => $this->plm,
            'status' => 3,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    # ~PANDING

    public function panding_list()
    {
        $data = [
            'title' => 'Project List :: Panding List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/project_list/panding_list', 'Panding List')],
            'smallTitle' => 'Panding List',
            'project' => $this->plm,
            'status' => 4,
        ];

        $this->render_page('page/project/project_list', $data);
    }

}