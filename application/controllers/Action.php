<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Action extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_pc');
        $this->load->model('MitraModel', 'mm');
        $this->load->model('StatementModel', 'sm');
        $this->load->model('DocumentModel', 'dm');
        $this->load->model('MSupport', 'support');
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function addPc()
    {
        $this->load->model('ProjectListModel', 'plm');
        $this->plm->in();
    }

    public function tampilPc()
    {
        $data['project'] = $this->M_pc;
        redirect('page/project_list', $data);
    }

    public function updatePc()
    {
        $id = $this->input->get('id');
        $task = $this->input->get('task');
        // $s_sts = $this->input->get('s_sts');
        // $s_tcel = $this->input->get('s_tcel');
        // $ket_s_sts = $this->input->get('ket_s_sts');
        // $ket_s_tcel = $this->input->get('ket_s_tcel');
        $status = $this->input->get('status');
        $pic_sts = $this->input->get('pic_sts');
        $pic_tcel = $this->input->get('pic_tcel');
        $note = $this->input->get('note');
        $review = $this->input->get('review');
        $confirm = $this->input->get('confirm');
        $via = $this->input->get('via');

        $data = [
            'task' => $task,
            // 's_sts' => $s_sts,
            // 's_tcel' => $s_tcel,
            // 'ket_s_sts' => $ket_s_sts,
            // 'ket_s_tcel' => $ket_s_tcel,
            'status' => $status,
            'pic_sts' => $pic_sts,
            'pic_tcel' => $pic_tcel,
            'note' => $note,
            'review' => $review,
            'confirm' => $confirm,
            'via' => $via,
        ];

        $this->db->update('pl', $data, ['id' => $id]);
        redirect('Project_List/project_list');
    }

    public function deletePc()
    {
        $id = $this->input->get('id');
        if ($id != '') {
            $id = explode(',', $id);
            $this->M_pc->de($id);
        }
        redirect('Project_List/project_list');
    }

    // MITRA

    public function inMitraJson()
    {
        $res = [];

        $q = $this->mm->inMitra();

        if ($q) {
            $res = [
                'msg' => 'Success to add mitra',
            ];
        } else {
            $res = [
                'msg' => 'Failed to add mitra',
            ];
        }

        // redirect($_SERVER['HTTP_REFERER']);
    }

    public function inMitra()
    {
        $q = $this->mm->inMitra();
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function inMitraList()
    {

        $id = $this->input->post('id_mitra');
        $q = $this->mm->deMitraListNormal(['id_project_list' => $this->input->post('id_pc')]);

        for ($i = 0; $i < count($id); $i++) {
            if ($q) {
                $cek = $this->mm->getMitraListID($id[$i], ['id_mitra' => $id[$i], 'id_project_list' => $this->input->post('id_pc')]);
                if ($cek->num_rows() > 0) {

                } else {
                    $object = [
                        'id_mitra' => $id[$i],
                        'id_project_list' => $this->input->post('id_pc'),
                        'created_date' => date('Y-m-d H:i:s'),
                    ];

                    $q = $this->mm->inMitraList($object);
                }
            }

        }

        redirect('Project_List/scope?id_pc=' . $this->input->post('id_pc'));
    }

    public function deMitra()
    {
        $q = $this->mm->deMitra();
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function upMitra()
    {
        $q = $this->mm->upMitra();
        if ($this->input->post('inp') != '') {
            redirect('Project_List/mitra?id_pc=' . $this->input->post('inp'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    // STATEMENT

    public function inStatement()
    {
        $q = $this->sm->inStatement();
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function inStatementList()
    {
        $id = $this->input->get('id_statement');
        for ($i = 0; $i < count($id); $i++) {
            $cek = $this->sm->getStatementListID($id[$i], ['id_statement' => $id[$i]]);
            if ($cek->num_rows() > 0) {

            } else {
                $object = [
                    'id_statement' => $id[$i],
                    'id_project_list' => $this->input->get('id_pc'),
                    'created_date' => date('Y-m-d H:i:s'),
                ];

                $q = $this->sm->inStatementList($object);
            }
        }

        redirect('Project_List/document?id_pc=' . $this->input->get('id_pc'));
    }

    public function deStatement()
    {
        $q = $this->sm->deStatement();
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function upStatement()
    {
        $q = $this->sm->upStatement();
        if ($this->input->post('inp') != '') {
            redirect('Project_List/statement?id_pc=' . $this->input->post('inp'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    // DOCUMENT

    public function inDocument()
    {
        $q = $this->dm->inDocument();
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function uploadDoc()
    {
        $type = 'jpg|png|xlsx|pdf|pptx';

        $pp = (array) json_decode($this->support->upload('pp', './uploads/pra_project/', $type));
        $op = (array) json_decode($this->support->upload('op', './uploads/on_project/', $type));
        $cl = (array) json_decode($this->support->upload('cl', './uploads/clossing/', $type));

        if ($pp['error'] != 1) {

            $object = [
                'id_project_list' => $this->input->post('id_pc'),
                'id_document' => 1,
                'file' => $pp['success']->file_name,
                'created_date' => date('Y-m-d H:i:s'),
            ];

            $this->dm->inDocumentList($object);

        }

        if ($op['error'] != 1) {

            $object = [
                'id_project_list' => $this->input->post('id_pc'),
                'id_document' => 2,
                'file' => $op['success']->file_name,
                'created_date' => date('Y-m-d H:i:s'),
            ];

            $this->dm->inDocumentList($object);

        }

        if ($cl['error'] != 1) {

            $object = [
                'id_project_list' => $this->input->post('id_pc'),
                'id_document' => 3,
                'file' => $cl['success']->file_name,
                'created_date' => date('Y-m-d H:i:s'),
            ];

            $this->dm->inDocumentList($object);

        }

        redirect('Project_List/project_list');
    }

}

/* End of file ProjectCharter.php */
/* Location: ./application/controllers/ProjectCharter.php */
