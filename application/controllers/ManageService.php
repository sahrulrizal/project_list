<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManageService extends MY_Controller
{

    private $bread = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('CDRModel', 'cdr');
        $this->load->model('ServiceModel', 's');
        $this->load->model('LogDailyActivityModel', 'lda');
        $this->load->model('LogDailyDeviceModel', 'ldd');
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Manage Service',
        ];
        $this->render_page('page/dashboard/index', $data);
    }

    // CDR
    public function customerDailyRequest()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/customerDailyRequest', 'Customer Daily Request List')],
            'title' => 'Manage Service :: Costumer Daily Request',
            'smallTitle' => 'Costumer Daily Request',
            'model' => $this->cdr,
        ];
        $this->render_page('page/manage_service/customerDailyRequest', $data);
    }

    public function dtCdr()
    {
        echo $this->cdr->dtCdr();
    }

    public function inCDR()
    {
        $log = [];

        $in = $this->cdr->inCDR();
        if ($in) {
            $log = [
                'msg' => 'Berhasil Menambahkan Request',
            ];
        } else {
            $log = [
                'msg' => 'Gagal Menambahkan Request',
            ];
        }

        echo json_encode($log);
    }

    public function getCDRID()
    {
        echo json_encode($this->cdr->getCDRID()->row());
    }
    public function getservive()
    {
        echo json_encode($this->s->getservice()->row());
    }

    public function upCDR()
    {
        $id = $this->input->post('id');

        $obj = [
            'request_date' => $this->input->post('request_date'),
            'costumer_request' => $this->input->post('costumer_request'),
            'whom' => $this->input->post('whom'),
            'status' => $this->input->post('status'),
            'request_done' => $this->input->post('request_done'),
            'request_by' => $this->input->post('request_by'),
            'review' => $this->input->post('review'),
            'note' => $this->input->post('note'),
        ];

        echo json_encode($this->cdr->upCDR($obj, $id));
    }

    // SERVICES
    public function services()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/services', 'Services')],
            'title' => 'Manage Service :: Service',
            'smallTitle' => 'Service',
            'model' => $this->s,
        ];
        $this->render_page('page/manage_service/services', $data);
    }

    public function dtservice()
    {
        echo $this->s->dtservice();
    }

    public function upservices()
    {
        $id = $this->input->post('id');

        $obj = [
            'request_date' => $this->input->post('request_date'),
            'services' => $this->input->post('services'),
            'whom' => $this->input->post('whom'),
            'status' => $this->input->post('status'),
            'request_done' => $this->input->post('request_done'),
            'request_by' => $this->input->post('request_by'),
            'review' => $this->input->post('review'),
            'note' => $this->input->post('note'),
        ];

        echo json_encode($this->s->upServices($obj, $id));
    }

    public function getServiceID()
    {
        echo json_encode($this->s->getServicesID()->row());
    }

    public function inServices()
    {
        $log = [];

        $in = $this->s->inServices();
        if ($in) {
            $log = [
                'msg' => 'Berhasil Menambahkan Services',
            ];
        } else {
            $log = [
                'msg' => 'Gagal Menambahkan Services',
            ];
        }

        echo json_encode($log);
    }

    // SERVICES
    public function logDailyActivity()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/logDailyActivity', 'Log Daily Activity')],
            'title' => 'Manage Service :: Log Daily Activity',
            'smallTitle' => 'Log Daily Activity',
            'model' => $this->lda,
        ];

        $this->render_page('page/manage_service/logDailyActivity', $data);
    }

    public function dtDA()
    {
        echo $this->lda->dtDA();
    }

    public function getLDAID()
    {
        // $id = $this->input->get('id');
        $in = $this->input->get('in');
        $tanggal = $this->input->get('tanggal');

        // $da = $this->lda->getDA($id);
        // $da = $da->row();
        // $status = $da->status;

        // $lda = $this->lda->getLDA('','',['id_da' => $id, 'created_date' => $tanggal]);

        // if ($lda->num_rows() > 0) {
        //     $lda = $lda->row();
        //     $status = $lda->status;
        // }

        $logs = [];

        $query = "SELECT * FROM daily_activity da WHERE da.id in ($in)";
        $q = $this->lda->getDA('', $query)->result();
        foreach ($q as $da) {
            $log = [
                'id' => $da->id,
                'description' => $da->description,
                'pic' => $da->pic,
                'status' => $this->getStatusLDA($da->id, $da->status, $tanggal),
                'note' => $da->note,
                'review' => $da->review,
            ];

            array_push($logs, $log);
        }

        echo json_encode($logs);

    }

    public function getStatusLDA($id = '', $s = '', $tanggal = '')
    {
        $lda = $this->lda->getLDA('', '', ['id_da' => $id, 'created_date' => $tanggal]);
        if ($lda->num_rows() > 0) {
            $lda = $lda->row();
            $status = $lda->status;
        } else {
            $status = $s;
        }

        return $status;
    }

    public function upLDAOne()
    {
        $log = [];
        $idPost = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');

        $status = $this->input->post('field') == 'status' ? $this->input->post('val') : '';

        $obj = [
            $this->input->post('field') => $this->input->post('val'),
        ];

        $id = ['id' => $idPost];

        $in = $this->lda->upLDAOne($obj, $id);
        if ($in) {
            $log = [
                'msg' => 'Berhasil Ubah Device',
            ];

            $OLDA = [
                'id_da' => $idPost,
                'status' => $status,
                'created_date' => $tanggal,
            ];

            $deLog = $this->lda->deLDA('', ['id_da' => $idPost, 'created_date' => $tanggal]);
            if ($deLog) {
                $inLog = $this->lda->inLDA($OLDA);
            }

        } else {
            $log = [
                'msg' => 'Gagal Ubah Device',
            ];
        }

        echo json_encode($log);
    }
    public function upLDDOne()
    {
        $log = [];
        $idPost = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');

        $status = $this->input->post('field') == 'status' ? $this->input->post('val') : '';

        $obj = [
            $this->input->post('field') => $this->input->post('val'),
        ];

        $id = ['id' => $idPost];

        $in = $this->ldd->upLDDOne($obj, $id);
        if ($in) {
            $log = [
                'msg' => 'Berhasil Ubah Device',
            ];

            $OLDA = [
                'id' => $idPost,
                'status' => $status,
                'created_date' => $tanggal,
            ];

            $deLog = $this->ldd->deLDD('', ['id' => $idPost, 'created_date' => $tanggal]);
            if ($deLog) {
                $inLog = $this->ldd->inLDD($OLDA);
            }
        } else {
            $log = [
                'msg' => 'Gagal Ubah Device',
            ];
        }

        echo json_encode($log);
    }

    // DMDD
    public function dailyDevice()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/dailyDevice', 'Log Daily Device')],
            'title' => 'Manage Service :: Log Daily Device',
            'smallTitle' => 'Log Daily Device',
            'model' => $this->ldd,
        ];

        $this->render_page('page/manage_service/logDailyDevice', $data);
    }

    public function dtDD()
    {
        echo $this->ldd->dtDD();
    }
    // crud
    public function getData()
    {
        $in = $this->input->get('in');
        $tanggal = $this->input->get('tanggal');

        $logs = [];

        $query = "SELECT * FROM daily_device da WHERE da.id in ($in)";
        // $q = $this->ldd->getDA('', $query)->result();
        $q = $this->ldd->getDD('', $query)->result();
        foreach ($q as $dd) {
            $log = [
                'id' => $dd->id,
                'date' => $dd->date,
                'device' => $dd->device,
                'status' => $this->getStatusLDD($dd->id, $dd->status, $tanggal),
                'note' => $dd->note,
                'created_date' => $dd->created_date,
            ];

            array_push($logs, $log);
        }

        echo json_encode($logs);
    }

    public function getStatusLDD($id = '', $s = '', $tanggal = '')
    {
        $ldd = $this->ldd->getDD('', '', ['id' => $id, 'created_date' => $tanggal]);
        if ($ldd->num_rows() > 0) {
            $ldd = $ldd->row();
            $status = $ldd->status;
        } else {
            $status = $s;
        }

        return $status;
    }

    public function udpate()
    {
        $id = $this->input->post('id');
        $date = date('Y-m-d H:i:s');
        $device = $this->input->post('device');
        $status = $this->input->post('status');
        $note = $this->input->post('note');
        $created_date = date('Y-m-d H:i:s');
        $data = $this->MJ->updatemeja($id, $date, $device, $status, $note, $created_date);
        echo json_encode($data);
    }
}