<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    private $bread = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('ProjectListModel','pl');

        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Dashboard'
        ];
        
        $this->render_page('page/dashboard/index', $data);
    }

    public function dataDashboard()
    {
        $tahun = $this->input->get('tahun');
        
        echo json_encode($this->pl->dashboard($tahun));
    }

}